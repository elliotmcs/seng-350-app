import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-usecase',
  templateUrl: './usecase.component.html',
  styleUrls: ['./usecase.component.css']
})
export class UsecaseComponent implements OnInit {
  usecase: any;
  pid: string;
  uid: string;
  ucid: string;
  title: string;

  constructor(private newService: CommonService,
    private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    console.log('usecase component init');
    // get uid from url
    this.uid = this.route.snapshot.paramMap.get('id');
    // get ucid from url
    this.ucid = this.route.snapshot.paramMap.get('ucid');
    this.getUsecase();
  }

  getUsecase(): void {
    // Get use case with uid
    this.newService.getUC(this.ucid).subscribe(usecase => {
      if (usecase !== undefined) {
        this.usecase = usecase[0];
        // Initialize inner_data if it is null/undefined
        if (this.usecase.inner_data === undefined || this.usecase.inner_data === null) {
          this.usecase.inner_data = {goal: '',
            scope: '',
            preconditions: '',
            success: '',
            failure: '',
            pactor: '',
            sactor: '',
            _trigger: '',
            description: '',
            extentions: '',
            subvar: ''};
        }
      }
    });
  }


  saveUseCase(): void {
    const result = confirm('Save changes to ' + this.usecase.title + '?');
    // Save updated data by sending update api request
    if (result) {
      this.newService.saveUC({mode: 'update', ucid: this.ucid, title: this.usecase.title, inner_data: this.usecase.inner_data}).subscribe();
      this.router.navigate(['/user/' + this.uid]);
    }
  }

  cancel(): void {
    const result = confirm('Cancel changes?');
    if (result) {
      this.router.navigate(['/user/' + this.uid]);
    }
  }

  public delete(): void{
    this.newService.deleteUC(this.ucid)
      .subscribe();
    this.router.navigate(['user/' + this.uid]);

  }
}
