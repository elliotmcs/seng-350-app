import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormsModule} from '@angular/forms';
import { CommonService } from '../common.service';
import { v4 as uuid } from 'uuid';


import { HttpClient, HttpResponse, HttpHeaders, HttpRequest} from '@angular/common/http';

@Component({
  selector: 'app-delete-user',
  templateUrl: './deleteUser.component.html'
})
export class DeleteUserComponent implements OnInit {
  private currentId: string;
  constructor(private newService: CommonService ) { }
  Repdata;

  ngOnInit() {
    this.currentId = uuid();
    this.newService.getUsers().subscribe(data => this.Repdata = data);
  }
  delete = function(id) {
    this.newService.deleteUser(id)
      .subscribe(data => {this.ngOnInit(); }, error => this.errorMessage = error);
  };
}
