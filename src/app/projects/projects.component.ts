import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../common.service';
import {v4 as uuid} from 'uuid';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  project: any;
  usecases: any;
  pid: string;
  uid: string;
  title: string;
  description: string;
  invite_user_id: number;
  surrender_user_id: number;
  users: any;
  owner: boolean;

  constructor(private newService: CommonService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    console.log('project component init');
    // Get pid from url
    this.pid = this.route.snapshot.paramMap.get('pid');
    // Get uid from url
    this.uid = this.route.snapshot.paramMap.get('id');
    this.getProject();
    this.getUsers();
  }

  getProject(): void {
    // Get use cases with uid and pid
    this.newService.getUCsByPID(this.uid, this.pid)
      .subscribe(usecases => this.usecases = usecases);

    // Get project with pid related to uid

    this.newService.getProject(this.uid, this.pid)
      .subscribe((project: Array<any>) => {
        if (project.length > 0) {
          this.project = project[0];
          this.owner = (this.project.owner === this.uid);
        }
      });
  }

  getUsers(): void {
    this.newService.getUsers().subscribe(users => this.users = users);
  }

  public surrenderProject(user_id: string): void {
    if (user_id) {
      const result = confirm('Are you sure that you want to surrender your project to user id: ' + user_id + '?');
      if (result) {
        this.newService.surrender(this.uid, user_id, this.pid).subscribe();
      }
    } else {
      const result = confirm('You must enter a user id before proceeding');
    }
  }

  public inviteUser(user_id: string): void {
    if (user_id) {
      const result = confirm('Are you sure that you want to invite user id: ' + user_id + '?');
      if (result) {
        this.newService.invite(user_id, this.pid).subscribe();
      }
    } else {
      const result = confirm('You must enter a user id before proceeding');
    }
  }
  public createUC(): void {
    const ucid = uuid();
    this.newService.saveUC({ mode: 'Save', uid: this.uid, pid: this.pid, ucid: ucid, title: '' })
      .subscribe();
    this.router.navigate(['/user/' + this.uid + '/usecases/' + ucid]);
  }
}
