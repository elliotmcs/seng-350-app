import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UseCaseDashboardComponent } from './usecase.dashboard.component';

describe('UseCaseDashboardComponent', () => {
  let component: UseCaseDashboardComponent;
  let fixture: ComponentFixture<UseCaseDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UseCaseDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UseCaseDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
