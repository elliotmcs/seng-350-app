import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../common.service';
import {v4 as uuid} from 'uuid';

@Component({
  selector: 'app-usecase',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  usecase: any;
  pid: string;
  uid: string;
  ucid: string;
  title: string;

  constructor(private newService: CommonService,
              private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    console.log('create component init');
    // get uid from url
    this.uid = this.route.snapshot.paramMap.get('id');
   // get pid from url
    this.pid = this.route.snapshot.paramMap.get('pid');
  }


  onSaveUC = function(user/*, isValid: boolean*/) {
    user.mode = this.valbutton;
    this.newService.saveUC(user)
      .subscribe(data => { alert(data.data);
        this.ngOnInit();
      }, error => this.errorMessage = error);
  };
  deleteUC = function(pid, id) {
    this.newService.deleteUC(this.uid, pid, id)
      .subscribe(data => {alert(data); this.ngOnInit(); }, error => this.errorMessage = error);
  };
  saveUC = function(kk) {
    const ucid = uuid();
    this.uctitle = kk.title;
    console.log('kk.pid: ' + kk.pid + 'selectedPid: ' + this.selectedPid + ' ucid: ' + ucid);
    this.newService.saveUC({ mode: 'Save', uid: this.uid, title: this.uctitle, pid: this.pid, ucid: ucid })
      .subscribe(data => {alert(data); this.ngOnInit(); }, error => this.errorMessage = error);
  };
}
