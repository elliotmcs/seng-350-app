import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormsModule} from '@angular/forms';
import { CommonService } from '../common.service';
import { v4 as uuid } from 'uuid';

import { HttpClient, HttpResponse, HttpHeaders, HttpRequest} from '@angular/common/http';

@Component({
  selector: 'app-add-user',
  templateUrl: './addUser.component.html'
})
export class AddUserComponent implements OnInit {
  private currentId: string;
  constructor(private newService: CommonService ) { }
  Repdata;
  valbutton = 'Save';

  ngOnInit() {
    this.currentId = uuid();
    this.newService.getUsers().subscribe(data => this.Repdata = data);
  }


  save = function(kk) {
    this.currentId = uuid();
    this.name = kk.name;
    this.admin = kk.admin;
    if (this.admin !==  true) {
      this.admin = false;
    }
    console.log('uid: ' + this.currentId + ' name: ' + this.name + ' admin: ' + this.admin);
    this.newService.saveUser({ mode: 'Save', uid: this.currentId, name: this.name, admin: this.admin })
      .subscribe(data => {this.ngOnInit(); }, error => this.errorMessage = error);
  };

}
