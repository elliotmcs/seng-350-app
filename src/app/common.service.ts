import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  constructor(private http: HttpClient) { }

  saveUser(user) {
    return this.http.post('http://localhost:8080/api/SaveUser/', user)
      .pipe();
  }

  getUsers() {
    return this.http.get('http://localhost:8080/api/getUsers/')
      .pipe();
  }

  getUser(id) {
    return this.http.get('http://localhost:8080/api/getUser?uid=' + id)
      .pipe();
  }

  deleteUser(id) {
    return this.http.post('http://localhost:8080/api/deleteUser/', {'id': id})
      .pipe();
  }

  saveProject(project) {
    return this.http.post('http://localhost:8080/api/SaveProject/', project)
      .pipe();
  }

  getProjects() {
    return this.http.get('http://localhost:8080/api/getProjects/')
      .pipe();
  }

  getProjectsByUID(uid){
    return this.http.get( 'http://localhost:8080/api/getProjects?uid=' + uid)
      .pipe();
  }

  getProject(uid, pid) {
    return this.http.get('http://localhost:8080/api/getProject?uid=' + uid + '&pid=' + pid)
      .pipe();
  }

  deleteProject(uid, pid) {
    return this.http.post('http://localhost:8080/api/deleteProject/', {'uid': uid, 'pid': pid})
      .pipe();
  }

  saveUC(usecase) {
    return this.http.post('http://localhost:8080/api/SaveUC/', usecase)
      .pipe();
  }

  getAllUC() {
    return this.http.get('http://localhost:8080/api/getUCs')
      .pipe();
  }

  getUCsByUID(uid) {
    return this.http.get('http://localhost:8080/api/getUCs?uid=' + uid)
      .pipe();
  }

  getUCsByPID(uid, pid) {
    return this.http.get('http://localhost:8080/api/getUCs?uid=' + uid + '&pid=' + pid)
      .pipe();
  }

  getUC(ucid) {
    return this.http.get('http://localhost:8080/api/getUC?ucid=' + ucid)
      .pipe();
  }

  deleteUC(ucid) {
    return this.http.post('http://localhost:8080/api/deleteUC/', {'ucid': ucid})
      .pipe();
  }

  surrender(uid1, uid2, pid) {
    return this.http.post('http://localhost:8080/api/surrenderProject/', {'uid1': uid1, 'uid2': uid2, 'pid': pid})
      .pipe();
  }

  invite(uid, pid) {
    return this.http.post('http://localhost:8080/api/inviteUser', {'uid': uid, 'pid': pid})
      .pipe();
  }

}
