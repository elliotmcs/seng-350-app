# MyApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Open project in Webstorm. Ensure the following Run/Debug configurations:
* Run/Debug type: Angular CLI Server
* package.json: [INSTALL DIRECTORY]/Angular-app/package.json
* Command: Run
* Script: start
* Arguments: None
* Node interpreter: [NODE EXECUTABLE PATH]
* Node options: None
* Package Manager: [NPM EXECUTABLE PATH]
* Environment: None

Next start the server: <br>
$ node src/server.js

Run mongodb with dbpath specified: <br>
$ mongod --dbpath [INSTALL DIRECTORY]/Angular-app/data-base 

Finally Run Angular CLI Server in Webstorm, or: <br>
$ ng serve

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
