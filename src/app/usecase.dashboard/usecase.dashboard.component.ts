import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CommonService} from '../common.service';

@Component({
  selector: 'app-usecase-dashboard',
  templateUrl: './usecase.dashboard.component.html',
  styleUrls: [ './usecase.dashboard.component.css' ]
})
export class UseCaseDashboardComponent implements OnInit {
  usecases: any;
  uid: string;
  constructor(private route: ActivatedRoute,
  private newService: CommonService) { }

  ngOnInit() {
    this.getUseCases();
    document.write(JSON.stringify(this.usecases[0]));
  }

  getUseCases(): void {
    // Get user Id from url
    this.uid = this.route.snapshot.paramMap.get('id');

    // Get use cases related to the uid
    this.newService.getUCsByUID(this.uid)
      .subscribe(usecases => this.usecases = usecases);
  }

  testbutton(): void {
    document.write(JSON.stringify(this.usecases));
  }
}
