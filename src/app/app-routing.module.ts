import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProjectDashboardComponent } from './project.dashboard/project.dashboard.component';
import {LoginComponent} from './login/login.component';
import {UserComponent} from './user/user.component';
import {DeleteUserComponent} from './deleteUser/deleteUser.component';
import {AddUserComponent} from './addUser/addUser.component';
import {ProjectsComponent} from './projects/projects.component';
import {UseCaseDashboardComponent} from './usecase.dashboard/usecase.dashboard.component';
import {EditProjectComponent} from './editproject/editproject.component';
import {CreateProjectComponent} from './createproject/createproject.component';
import {UsecaseComponent} from './usecase/usecase.component';
import {CreateComponent} from './create/create.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent},
  { path: 'user/:id/projects', component: ProjectDashboardComponent },
  { path: 'user/:id/usecases', component: UseCaseDashboardComponent },
  { path: 'user/:id/projects/:pid', component: ProjectsComponent },
  { path: 'user/:id/usecases/:ucid', component: UsecaseComponent},
  { path: 'user/:id', component: UserComponent },
  { path: 'deleteUser', component: DeleteUserComponent },
  { path: 'addUser', component: AddUserComponent},
  { path: 'user/:id/editProject/:pid', component: EditProjectComponent},
  {path: 'user/:id/newProject', component: CreateProjectComponent},
  {path: 'user/:id/projects/:pid/new', component: CreateComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
