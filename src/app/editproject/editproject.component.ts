import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommonService} from '../common.service';

@Component({
  selector: 'app-editproject',
  templateUrl: 'editproject.component.html',
  styleUrls: [ './editproject.component.css' ]
})
export class EditProjectComponent implements OnInit {
  project: any;
  pid: string;
  uid: string;
  title: string;
  description: string;
  UCData;

  constructor(private newService: CommonService,
              private route: ActivatedRoute,
              private router: Router) {}

  ngOnInit(): void {
    // Get pid from url
    this.pid = this.route.snapshot.paramMap.get('pid');
    // Get uid from url
    this.uid = this.route.snapshot.paramMap.get('id');
    this.getProject();
    this.newService.getUCsByUID(this.uid).subscribe( data => this.UCData = data);
  }

  getProject(): void {
    this.newService.getProject(this.uid, this.pid)
      .subscribe((project: Array<any>) => {
        if (project.length > 0) {
          this.project = project[0];
        }
      });
  }

  onSaveProject = function(user) {
    user.mode = this.valbutton;
    this.newService.saveProject(user)
      .subscribe(data => { alert(data.data);
        this.ngOnInit();
      }, error => this.errorMessage = error);
  };

  saveProject = function(input) {
    this.newService.saveProject({ mode: 'Update', uid: this.uid, title: input.title, pid: this.pid, description: input.description})
      .subscribe(data => {alert(data); this.ngOnInit(); }, error => this.errorMessage = error);
  };

  deleteProject = function(id) {
    this.newService.deleteProject(this.uid, this.pid)
      .subscribe(data => this.ngOnInit(), error => this.errorMessage = error);
  };

  dProject = function() {
    this.deleteProject();
    this.router.navigate(['/user/' + this.uid]);
  };

  eProject = function(input) {
    this.deleteProject();
    this.saveProject(input);
    this.router.navigate(['/user/' + this.uid]);
  };
}
