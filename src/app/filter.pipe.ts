import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(values: any[], search: string): any {
    // let arr;
    if (search === undefined) {
      search = '';
    }
    let i: number = values.length;
    for (i >= 1; i--;) {
      // document.write(JSON.stringify(values[i]['title']));
      if (values[i] !== undefined) {
        if (values[i]['title'].toLowerCase().includes(search.toLowerCase())) {
        } else {
          delete values[i];
        }
      }
    }
    // document.write(JSON.stringify(values));
    return values;
  }
}
