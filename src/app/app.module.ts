import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ProjectDashboardComponent } from './project.dashboard/project.dashboard.component';

import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import {UserComponent} from './user/user.component';
import {DeleteUserComponent} from './deleteUser/deleteUser.component';
import {AddUserComponent} from './addUser/addUser.component';
import {ProjectsComponent} from './projects/projects.component';
import {UseCaseDashboardComponent} from './usecase.dashboard/usecase.dashboard.component';
import { EditProjectComponent } from './editproject/editproject.component';
import { CreateProjectComponent } from './createproject/createproject.component';
import { FilterPipe } from './filter.pipe';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {UsecaseComponent} from './usecase/usecase.component';
import { CreateComponent } from './create/create.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule.forRoot()
  ],
  declarations: [
    AppComponent,
    ProjectDashboardComponent,
    UseCaseDashboardComponent,
    LoginComponent,
    UserComponent,
    ProjectsComponent,
    DeleteUserComponent,
    AddUserComponent,
    EditProjectComponent,
    CreateProjectComponent,
    FilterPipe,
    UsecaseComponent,
    CreateComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
