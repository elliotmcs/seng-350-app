var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var mongo = require('mongoose');

var db = mongo.connect('mongodb://seng350:group5@ds261917.mlab.com:61917/seng350', function(err, response){
  if(err){ console.log(err);}
  else{console.log('Connected to ' + db, ' + ', response);}
});

var app = express();
app.use(bodyParser());
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({extended: true}));

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
  res.setHeader('Access-Control-Allow-Methods', 'Get, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With,content-type, Accept');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

var Schema = mongo.Schema;

var UserSchema = new Schema({
  uid: { type: String },
  name: { type: String },
  admin: { type: Boolean },
},{ versionKey: false });

var ProjectSchema = new Schema({
  owner: { type: String },
  editors: [ String ],
  title: { type: String},
  pid: {type: String},
  description: {type: String}
});

var UseCaseSchema = new Schema({
  editors: [ String ],
  title: {type: String},
  pid: {type: String},
  ucid: {type: String},
  inner_data: {type: Object}
});

var userModel = mongo.model('users', UserSchema, 'users');

app.post('/api/SaveUser', function(req, res){
  var mod = new userModel(req.body);
  if(req.body.mode === "Save"){
    mod.save(function(err, data){
      if(err){
        res.send(err);
      }else{
        res.send({data:"Record has been Inserted"});
      }
    });
  }else{
    userModel.findByIdAndUpdate(req.body.id, {uid: req.body.uid, name: req.body.name, admin: req.body.admin},
      function(err, data) {
        if(err){
          res.send(err);
        }else{
          res.send({data: "Record has been Updated"});
        }
      });
  }
})

app.post("/api/deleteUser", function(req,res){
  userModel.deleteOne({_id: req.body.id}, function(err){
    if(err){
      res.send(err);
    }else{
      res.send({data: "Record has been Deleted"});
    }
  });
})

app.get("/api/getUsers", function(req,res){
  userModel.find({}, function(err, data){
    if(err){
      res.send(err);
    }else{
      res.send(data);
    }
  });
});

app.get("/api/getUser", function(req,res){
  userModel.find({uid: req.query.uid}, function(err, data){
    if(err){
      res.send(err);
    }else{
      res.send(data);
    }
  });
});

var projectModel = mongo.model('projects', ProjectSchema, 'projects');

app.post('/api/SaveProject', function(req, res){
  if(req.body.mode === "Save"){
    var mod = new projectModel({editors: [req.body.uid], pid: req.body.pid, owner: req.body.uid, title: req.body.title, description: req.body.description});
    mod.save(function(err, data){
      if(err){
        console.log(err);
        res.send(err);
      }else{
        res.send({data:"Record has been Inserted"});
      }
    });
  }else{
    projectModel.update({editors: req.body.uid, pid: req.body.pid}, {$set: {description: req.body.description, title: req.body.title}}, function(err, data) {
        if(err){
          console.log("error");
          console.log(err);
          res.send(err);
        }else{
          console.log("done: \n"+data);
          res.send({data: "Record has been Updated"});
        }
      });
  }
})

app.post("/api/deleteProject", function(req,res){
  projectModel.remove({editors: req.body.uid, pid: req.body.pid}, function(err){
    if(err){
      res.send(err);
    }else{
      res.send({data: "Record has been Deleted"});
    }
  });
})

app.get("/api/getProjects", function(req,res){
  if(req.query.uid !== undefined) {
    projectModel.find({editors: req.query.uid }, function (err, data) {
      if (err) {
        res.send(err);
      } else {
        console.log(data);
        res.send(data);
      }
    });
  }else{
    projectModel.find({}, function (err, data) {
      if (err) {
        res.send(err);
      } else {
        res.send(data);
      }
    });
  }
});

app.get("/api/getProject", function(req,res){
  projectModel.find({editors: req.query.uid, pid: req.query.pid}, function(err, data){
    if(err){
      res.send(err);
    }else{
      res.send(data);
      console.log(data);
    }
  });
});

var usecaseModel = mongo.model('usecases', UseCaseSchema, 'usecases');

app.post('/api/SaveUC', function(req, res){
  if(req.body.mode === "Save"){
    var mod = new usecaseModel({editors: [req.body.uid], pid: req.body.pid, title: req.body.title, ucid: req.body.ucid, inner_data: req.body.data});
    mod.save(function(err, data){
      if(err){
        res.send(err);
      }else{
        res.send({data:"Record has been Inserted"});
      }
    });
  }else{
    console.log("ucid: " + req.body.ucid + "\ninner_data: " + req.body.inner_data);
    usecaseModel.updateOne({ucid: req.body.ucid}, {title: req.body.title, inner_data: req.body.inner_data},
      function(err, data) {
        if(err){
          res.send(err);
        }else{
          console.log("updateed with data: " + data);
          res.send({data: "Record has been Updated"});
        }
      });
  }
})

app.post("/api/deleteUC", function(req,res){
  usecaseModel.deleteOne({ucid: req.body.ucid}, function (err) {
    if (err) {
      res.send(err);
    } else {
      res.send({data: "Record has been Deleted"});
    }
  });
})

app.get("/api/getUCs", function(req,res){
  if(req.query.uid !== undefined) {
    if(req.query.pid !== undefined) {
      usecaseModel.find({editors: req.query.uid, pid: req.query.pid}, function (err, data) {
        if (err) {
          res.send(err);
        } else {
          console.log(data);
          res.send(data);
        }
      });
    }else{
      usecaseModel.find({editors: req.query.uid}, function (err, data) {
        if (err) {
          res.send(err);
        } else {
          console.log(data);
          res.send(data);
        }
      });
    }
  }else{
    usecaseModel.find({}, function (err, data) {
      if (err) {
        res.send(err);
      } else {
        res.send(data);
      }
    });
  }
});

app.get("/api/getUC", function(req,res){
  usecaseModel.find({ucid: req.query.ucid}, function(err, data){
    if(err){
      res.send(err);
    }else{
      console.log(data);
      res.send(data);
    }
  });
});

app.post("/api/surrenderProject", function(req,res){
  projectModel.updateOne({editors: req.body.uid1, pid: req.body.pid}, {$set: {owner: req.body.uid2}, $push: {editors: req.body.uid2}}, function(err, data){
    if(err){
      res.send(err);
    }else{
      console.log(data);
      res.send(data);
    }
  })
})

app.post("/api/inviteUser", function(req,res){
 projectModel.update({$push: {editors: req.body.uid}, pid: req.body.pid}, function(err, data){
   if(err){
     res.send(err);
   }
 });
})

app.listen(8080, function(){
  console.log("Example app listening on port 8080");
})
