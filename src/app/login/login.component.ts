import { Component, OnInit } from '@angular/core';
import {CommonService} from '../common.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.css' ]
})
export class LoginComponent implements OnInit {
  users: any;
  constructor(private commonService: CommonService) { }

  ngOnInit() {
    this.getUsers();

    // Create script element for selection list script
    const script_el = document.createElement('script');

    // Injected selection list script
    script_el.innerText = 'var activeElement = null;function onClick(el){if(activeElement == null || el !== activeElement){' +
      'if(activeElement != null)activeElement.style.backgroundColor = "#FFFFFF00";' +
      'activeElement = el;' +
      'el.style.backgroundColor = "#a0a0a0";' +
      '}' +
      'else if(activeElement != null && activeElement === el){' +
      'activeElement = null;' +
      'el.style.backgroundColor = "#FFFFFF00";' +
      '}}' +
      'function login(){' +
      'if(activeElement != null){' +
      'var result = confirm("Confirm Login");' +
      'if(result){' +
      'window.location.href=\'/user/\'+activeElement.id.slice(5,-1);' +
      '}else{' +
      'activeElement.style.backgroundColor = "#FFFFFF00";' +
      'activeElement = null;' +
      '}' +
      '}' +
      '}';
    document.getElementsByTagName('head')[0].appendChild(script_el);
  }

  getUsers(): void {
    // Get all users
    this.commonService.getUsers().subscribe(users => this.users = users);
  }

}
