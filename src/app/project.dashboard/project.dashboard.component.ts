import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CommonService} from '../common.service';

@Component({
  selector: 'app-project-dashboard',
  templateUrl: './project.dashboard.component.html',
  styleUrls: [ './project.dashboard.component.css' ]
})
export class ProjectDashboardComponent implements OnInit {
  projects: any;
  uid: any;
  state = 0;

  constructor(private route: ActivatedRoute,
  private newService: CommonService) { }

  ngOnInit() {
    this.getProjects();
    this.uid = this.route.snapshot.paramMap.get('id');
  }

  getProjects(): void {
    // Get user ID from url
    this.uid = this.route.snapshot.paramMap.get('id');

    // Get projects related to uid
    this.newService.getProjectsByUID(this.uid)
      .subscribe(projects => this.projects = projects);
  }
  changeState(number): void{
    this.state = number;
    this.ngOnInit();
  }
}
