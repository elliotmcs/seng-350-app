import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CommonService} from '../common.service';
import {v4 as uuid} from 'uuid';

@Component({
  selector: 'app-createproject',
  templateUrl: 'createproject.component.html',
  styleUrls: [ './createproject.component.css' ]
})
export class CreateProjectComponent implements OnInit {
  pid: string;
  uid: string;
  title: string;
  description: string;

  constructor(private newService: CommonService,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    // Get uid from url
    this.uid = this.route.snapshot.paramMap.get('id');
  }

  onSaveProject = function(user) {
    user.mode = this.valbutton;
    this.newService.saveProject(user)
      .subscribe(data => { alert(data.data);
        this.ngOnInit();
      }, error => this.errorMessage = error);
  };

  saveProject = function(input) {
    const pid = uuid();
    this.title = input.title;
    this.description = input.description;
    console.log('uid: ' + this.uid + ' title: ' + this.ptitle);
    console.log(this.description);
    this.newService.saveProject({ mode: 'Save', uid: this.uid, title: this.title, pid: pid, description: this.description})
      .subscribe(data => {alert(data); this.ngOnInit(); }, error => this.errorMessage = error);
  };
}
