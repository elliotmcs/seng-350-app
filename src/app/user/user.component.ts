import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import {CommonService} from '../common.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: [ './user.component.css' ]
})
export class UserComponent implements OnInit {
  user: any;
  nProjects: number;
  nUseCases: number;
  Repdata;
  uid;

  constructor(
    private route: ActivatedRoute,
    private newService: CommonService,
    private router: Router
  ) {
  }

  projectView = true;

  ngOnInit(): void {
    this.uid = this.route.snapshot.paramMap.get('id');
    this.getUser();
    this.newService.getUser(this.uid).subscribe(data => this.Repdata = data);
  }

  getUser(): void {
    // Gets user ID from url
    const id = this.route.snapshot.paramMap.get('id');

    // Get the User with specified ID
    this.newService.getUser(id)
      .subscribe(user => this.user = user);
  }

  goBack(): void {
    const result = confirm('You are about to logout, confirm?');
    if(result){
      this.router.navigate(['/login']);
    }
  }
}


